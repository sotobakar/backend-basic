## Konsep Database

Database (basis data) adalah sebuah penyimpanan atau file yang terdiri dari skema dan juga data. Untuk mengakses sebuah Database, kita menggunakan sebuah software yang dinamakan Database Management System (DBMS).

DBMS adalah sebuah software yang melakukan manajemen database  agar dapat diakses dan dimodifikasi. Satu DBMS dapat mengelola banyak database. Tergantung kebutuhan, DBMS yang berbeda dapat mengakses database yang sama.

Dibandingkan dengan software penyimpanan data lain seperti Word (.docx), Excel (.xls), Notepad (.txt), dll. Software-software di atas biasanya ditujukan untuk end-user atau pengguna software, sedangkan DBMS digunakan oleh programmer. 

Menggunakan DBMS jauh lebih efisien dengan memanfaatkan operasi yang bernama **queries**. Dengan DBMS, sebuah aplikasi dapat mengakses database secara programmatic atau dengan menggunakan kodingan.

Manfaat DBMS
- Programmatic Access
- Reliabel
- Terstruktur

```
Yang kita gunakan = DBMS
Yang digunakan DBMS = Database
```

Contoh DBMS yang digunakan di dunia IT:
1. MySQL
2. MariaDB
3. PostgreSQL
4. MongoDB
5. Redis
6. Amazon DynamoDB

## Hubungan Database dengan Backend

Database seringkali dikaitkan dengan Backend. Sebuah backend bekerja sebagai **service** yang dapat digunakan oleh frontend. Database berfungsi sebagai penyimpanan data bagi backend.

Tanpa database, sebuah backend tidak dapat menyimpan data baik data dari client maupun data olahan dari backend itu sendiri.

Alurnya sebagai berikut:

```
Frontend <--> Backend <--> Database
```

Contohnya, sebuah backend untuk toko online memerlukan penyimpanan data untuk data pembeli seperti nama, alamat, dan lain-lain.

### Latihan

1. Selain data pembeli, kira-kira apa saja data yang perlu disimpan di database oleh backend toko online?
2. Apa perbedaan penyimpanan dengan file .txt dan database?

## Jenis Database

Pada umumnya ada 2 jenis database yang sering digunakan oleh dunia IT, yaitu Relasional dan Nonrelasional. Mereka memiliki karakteristik dan kegunaan yang berbeda

### Database Relasional

Database Relasional atau sering disebut SQL database, merupakan database yang dapat dioperasikan dengan menggunakan **SQL** atau **Structured Query Language**.

Karakteristik utamanya adalah cara bagaimana database menyimpan data. Database Relasional menyimpan data dalam bentuk terstruktur dan tabular. Informasi disimpan pada **tabel**, yang diumpamakan sebagai tempat penyimpanan. Misalnya, sebuah tabel **mahasiswa** menyimpan data-data mahasiswa.

Pada tabel, data disimpan dalam bentuk **kolom**, dimana sebuah **kolom** dapat mengandung nilai sesuai **tipe data** yang ditentukan.

Misalnya, sebuah kolom **nama** yang diatur dengan tipe data **VARCHAR(40)** dapat menerima kumpulan karakter dengan panjang maksimal 40 karakter.

Data-data tersebut disimpan dalam bentuk **rows** atau baris. Satu baris menandakan satu entri data. Untuk membuat satu baris, kita memerlukan nilai pada setiap **kolom** pada tabel.

![[Pasted image 20221106103611.png | 500]]

#### Arti Kata Relasional

Kenapa disebut "relational" database? Karena tabel-tabel ini dapat berhubungan satu sama lain dengan menggunakan **foreign key**. Pada umumnya sebuah tabel memiliki kolom yang digunakan untuk mengidentifikasi setiap baris, yang dinamakan **id**.

Sebagai visualisasi, kita anggap sebuah tabel berisi merk mobil. Lalu kita juga memiliki tabel yang berisi spesifikasi mobil. Pastinya setiap mobil memiliki sebuah merk. 

Tabel **merk_mobil**:

| id | merk          |
|----|---------------|
| 1  | toyota        |
| 2  | honda         |
| 3  | mercedes-benz |
| 4  | bmw           |

Tabel **mobil**:

| id | tipe     | tahun_rilis | id_merk |
|----|----------|-------------|---------|
| 1  | civic    | 2022        | 2       |
| 2  | C Class  | 2018        | 3       |
| 3  | fortuner | 2021        | 1       |
| 4  | avanza   | 2021        | 1       |
| 5  | S Class  | 2022        | ???     |
| 6  | brio     | 2022        | ???     |

Nilai dari **id** pada tabel **merk_mobil** akan disimpan pada kolom **id_merk** pada tabel **mobil**.

Nilai pada **id_merk** akan digunakan ketika menghubungkan tabel mobil dan tabel merk_mobil.

#### Latihan

1. Tolong isi nilai ??? pada tabel mobil sesuai dengan id pada tabel merk_mobil

#### Contoh tabel mahasiswa

Sebuah tabel **mahasiswa** yang berisi kolom **nama**, **nim**, dan **no_telp**:
1. **nama** VARCHAR(40)
2. **nim** CHAR (10)
3. **no_telp** VARCHAR(13)

#### Data mahasiswa (row)

**nama**: Patrick Santino
**nim**: 1910512012
**no_telp**: 081316472184

#### Latihan

Ada sebuah tabel **hewan** dengan struktur data sebagai berikut:
1. **nama** VARCHAR(50)
2. **spesies** VARCHAR(50)
3. **warna** VARCHAR(10)
4. **bisa_terbang** TINYINT
5. **jumlah_kaki** INTEGER(4)

Hint:
bisa_terbang diisi 1 apabila hewan tersebut bisa terbang, 0 jika sebaliknya.

Silahkan isi data tersebut berdasarkan hewan berikut
1. 🦍
2. 🦄
3. 🦘

### Database non-relasional

Database nonrelasional (noSQL) tidak menyimpan data dalam bentuk terstruktur (tabular), dan juga tidak bersifat relasional (hubungan antar tabel). Keberadaan database jenis ini melengkapi kelemahan dari database relasional, biasanya dalam segi performa, resiliensi dan juga kompleksitas data.

Karakteristik utama dari database ini adalah ketidakstrukturan data, yang menyebabkan data bersifat fleksibel dan dinamis. Kolom dapat bertambah atau berkurang tanpa mengikuti aturan tabel. Data disimpan dalam bentuk **collection** atau **document**. Sebuah **collection** dapat berisi beberapa object (biasanya bentuk JSON).

Database jenis ini tidak dapat dioperasikan menggunakan bahasa **SQL**. Mereka memiliki syntax sesuai aturan database tersebut.

Beberapa database NoSQL terpopuler saat ini adalah MongoDB, Redis, Cassandra, dan DynamoDB.

## MySQL

### Instalasi MySQL
1. Download di link ini [MySQL :: Download MySQL Installer](https://dev.mysql.com/downloads/installer/)
2. Silahkan ikuti langkah2 di installernya hingga selesai
3. Pastikan kalian sudah bisa mengakses menggunakan command berikut

```shell
mysql -u root --password=password_kalian
```

### Menjalankan MySQL

Coming soon

### Menggunakan MySQL

Coming soon

