const student = {
    name: "Jackson",
    grade: "8th",
    school: "Bunda Mulia",
    hobby: "Basketball",
    favoritePlayer: "Steph Curry"
}

// Template Literal
const kalimat1 = `Hi my name is ${student.name}, I am an ${student.grade} grader in ${student.school} school.`;
const kalimat2 = `My hobby is playing ${student.hobby}, and my favorite player is ${student.favoritePlayer}`;

// Fungsi

// 1. Fungsi yang menerima array berisi item string

/**
 * Logs strings from an array to the console.
 * @param {string[]} sentences
 * 
 */
function introduceYourself(sentences) {
    // Loop sentences
    sentences.forEach((sentence) => {
        console.log(sentence);
    });
}

// 2. Fungsi yang dapat mengubah property hobby pada object studnet

/**
 * Change the hobby property of student object.
 * @param {string} newHobby
 * @param {Object} student
 * @returns {Object} object with modified hobby property.
 */
const changeHobby = (newHobby, student) => {
    // Copy agar tidak mengubah object lama
    const copiedStudent = Object.assign({}, student);

    // Modify property
    copiedStudent.hobby = newHobby;

    return copiedStudent;
}

// 3. Fungsi yang mengembalikan angka yang berupa penjumlahan dari seluruh angka pada array.

/**
 * Returns a sum of all numbers in the array;
 * @param {number[]} array
 * @returns {number} Sum of n numbers in array of size n
 */
 const sumArray = (array) => {
    const initialValue = 0;
    const result = array.reduce(function (previousValue, currentValue) {
        return previousValue + currentValue;
    }, initialValue);

    return result;
}
// Tugas
// 1. Jalankan ketiga fungsi tersebut lalu screenshot hasilnya
// 2. Tambah fungsi bernama changeGrade untuk mengubah property "grade" pada object student.
// 3. Gunakan built-in method map() pada array untuk mengubah array berisi angka berikut menjadi format rupiah
// contoh: [20000, 7100000, 430000] -> ['Rp. 20.000,-', 'Rp. 7.100.000,-', 'Rp. 430.000,-']

// TODO: Kerjakan nomor 1 disini.


// TODO: Kerjakan nomor 2 disini.
const changeGrade = () => {

}

/** @type {number[]} */
const angkaMentah = [20000, 7100000, 430000, 749999];

/** @type {string[]} */
const angkaBaru = angkaMentah.map(function (angka) {
    // TODO: Kerjakan nomor 3 disini.
});
